package com.gitlab.candicey.aoc2023

import java.io.File

typealias PartRunnerBlock = Puzzle.PuzzlePartRunner.() -> Any?

fun parseInput(day: Int, test: Boolean = false) = parseInputText(day, test)
    .lines()
    .filter(String::isNotBlank)

fun parseInput(fileName: String) = parseInputText(fileName)
    .lines()
    .filter(String::isNotBlank)

fun parseInputText(day: Int, test: Boolean = false) = File("input/${padDay(day, test)}.txt")
    .readText()

fun parseInputText(fileName: String) = File("input/$fileName")
    .readText()

fun padDay(day: Int, test: Boolean = false) = day.toString().padStart(2, '0') + if (test) ".test" else ""

fun List<String>.trim() = map(String::trim)