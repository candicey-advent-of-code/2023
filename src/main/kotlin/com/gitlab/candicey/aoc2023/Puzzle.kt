package com.gitlab.candicey.aoc2023

import kotlin.time.measureTimedValue

fun puzzle(day: Int, block: Puzzle.() -> Unit) = Puzzle(day).block()

class Puzzle(val day: Int) {
    val puzzleParts = mutableListOf<PuzzlePart>()

    val run by lazy { PuzzleExecutor() }
    val test by lazy { PuzzleExecutor(".test", "Test") }

    operator fun Int.invoke(block: PartRunnerBlock) = part(block)

    infix fun Int.part(block: PartRunnerBlock) = puzzleParts.add(PuzzlePart(this, block))

    fun findPart(part: Int) = puzzleParts.find { it.part == part }

    class PuzzlePart(val part: Int, val run: PartRunnerBlock)

    class PuzzlePartRunner(val puzzlePart: PuzzlePart, val input: List<String>)

    inner class PuzzleExecutor(val inputSuffix: String = "", val name: String = "Part") {
        val input by lazy { parseInput("${padDay(day)}$inputSuffix.txt") }

        infix fun execute(part: Int) = measureTimedValue { findPart(part)?.run { PuzzlePartRunner(this, input).run() } }
        infix fun output(part: Int) = execute(part).also { println("$name $part: ${it.value} (${it.duration.inWholeMilliseconds}ms)") }
    }
}