package com.gitlab.candicey.aoc2023.day

import com.gitlab.candicey.aoc2023.puzzle

fun main() = puzzle(1) {
    fun String.firstDigit() = first(Char::isDigit)
    fun String.lastDigit() = last(Char::isDigit)

    1 {
        input
            .map { "${it.firstDigit()}${it.lastDigit()}" }
            .map(String::toInt)
            .sum()
    }

    2 {
        val digitTextMap = mapOf(
            "one"   to 1,
            "two"   to 2,
            "three" to 3,
            "four"  to 4,
            "five"  to 5,
            "six"   to 6,
            "seven" to 7,
            "eight" to 8,
            "nine"  to 9,
        )

        // replace all text with digits
        val mappedInput = input
            .map { line ->
                val final = StringBuilder()
                var startRegion = 0
                var endRegion = 0

                fun submitRegion() {
                    val region = line.substring(startRegion, endRegion)
                    val digit = digitTextMap[region]
                    if (digit != null) {
                        final.append(digit)
                        startRegion++
                        endRegion = startRegion
                    }
                }

                fun newRegion() {
                    startRegion++
                    endRegion = startRegion
                }

                while (startRegion < line.length) {
                    if (endRegion >= line.length) {
                        newRegion()
                        continue
                    }
                    val char = line[endRegion]

                    if (char.isLetter()) {
                        endRegion++
                        submitRegion()
                    } else {
                        if (startRegion == endRegion) {
                            final.append(char)
                        }
                        newRegion()
                    }
                }

                if (startRegion != 0) {
                    submitRegion()
                }

                final.toString()
            }

        mappedInput
            .map { "${it.firstDigit()}${it.lastDigit()}" }
            .map(String::toInt)
            .sum()
    }

    run output 1
    test output 1

    val test2 = PuzzleExecutor(".test_2", "Test")

    run output 2
    test2 output 2
}