package com.gitlab.candicey.aoc2023.day

import com.gitlab.candicey.aoc2023.puzzle
import com.gitlab.candicey.aoc2023.trim

fun main() = puzzle(2) {
    fun parseInput(input: List<String>) = input
        .map { it.split(":") }
        .map { gameData ->
            val gameId = gameData[0].removePrefix("Game ")
            val rounds = gameData[1].split(";").trim()

            gameId.toInt() to rounds.map { round -> round.split(",").trim().map { Cube.parse(it) } }
        }

    1 {
        val data = parseInput(input)

        val redLimit = 12
        val greenLimit = 13
        val blueLimit = 14

        val possibleGame = data
            .filterNot {
                it.second.any { round ->
                    round.any { (num, colour) ->
                        val limit = when (colour) {
                            Colour.RED -> redLimit
                            Colour.GREEN -> greenLimit
                            Colour.BLUE -> blueLimit
                        }

                        num > limit
                    }
                }
            }

        possibleGame.sumOf { it.first }
    }

    2 {
        val data = parseInput(input)

        data.sumOf { (_, gameData) ->
            val flatted = gameData.flatten()

            fun findMaximum(colour: Colour) = flatted.filter { it.second == colour }.maxOf { it.first }

            val maxRed = findMaximum(Colour.RED)
            val maxGreen = findMaximum(Colour.GREEN)
            val maxBlue = findMaximum(Colour.BLUE)

            maxRed * maxGreen * maxBlue
        }
    }

    test output 1
    run output 1

    test output 2
    run output 2
}

private data class Cube(val num: Int, val colour: Colour) {
    companion object Parser {
        fun parse(string: String) = string.split(" ").let { it[0].toInt() to Colour.fromString(it[1])!! }
    }
}

private enum class Colour(val string: String) {
    RED("red"),
    GREEN("green"),
    BLUE("blue");

    companion object {
        fun fromString(string: String) = entries.find { it.string == string }
    }
}